#ifndef TINYCONF_HPP_
#define TINYCONF_HPP_

/*! * * * * * * * * * * * * * * * * * * * *
 * TinyConf Library
 * @version 0.1
 * @file tinyconf.hpp
 * @author Maxime 'Stalker2106' Martens
 * @brief Single header for Config class
 * * * * * * * * * * * * * * * * * * * * */

#include <cstring>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <limits>
#include <unistd.h>

// Stl Containers
#include <vector>
#include <map>

/* include configuration of parser */
#include "tinyconf.config.hpp"

/* Everything is defined within stb:: scope */
namespace stb {

/*!
 * @class Config
 * @brief Main Config class: Defines the whole library
 */
class Config
{
public:
    /*! @brief Type used to represent associations in memory */
    typedef std::pair<std::string, std::string> association;
    /*! @brief Container used to store associations in memory */
    typedef std::map<std::string, std::string> associationMap;

    /*! @brief Exception thrown when a requested key does not exist */
    class undefined_key : public std::exception
    {
        public:
        undefined_key(const std::string &key) : _key(key) {}
        virtual const char *what() const throw() { return (("Key "+_key+" is undefined").c_str()); }

        private:
        std::string _key;
    };

    /*! @brief Exception thrown when no file is bound to configuration */
    class no_file_bound : public std::exception
    {
        public:
        virtual const char *what() const throw() { return ("The configuration has no associated file"); }
    };

    /*! @brief Exception thrown when system cannot open the configuration file */
    class bad_file : public std::exception
    {
        public:
        bad_file(const std::string &path) : _path(path) {}
        virtual const char *what() const throw() { return (("Cannot read/write file \""+_path+"\" (Insufficient permissions ?)").c_str()); }
        
        private:
        std::string _path;
    };

    //
    // Config class
    //

    /*! @brief Config empty constructor */
    Config() {}

    /*!
     * @brief Config standard constructor
     * @param path : The path where the file.cfg will reside
     * @param overwrite : If true, any existing file will be overwritten
     */
    Config(const std::string &path, bool overwrite = false) : _path(path)
    {
        if (overwrite)
        {
            destroy();
        }
        else //Load existing conf from disk
        {
            load();
        }
    }

    //
    // FILE MANAGEMENT
    //

    /*!
     * @brief Get path of associated configuration file
     * @return String containing the path of the current associated cfg file
     */
    std::string getPath()
    {
        return (_path);
    }

    /*!
     * @brief Set path of associated configuration file
     * @param path :  The path where the file.cfg will reside
     */
    void setPath(const std::string &path)
    {
        _path = path;
    }


    /*!
     * @brief Checks if configuration is empty
     * @return true if empty, false if not
     */
    bool empty()
    {
        return (_config.empty());
    }

    /*!
     * @brief Empties configuration keys and values in memory
     */
    void clear()
    {
        _config.clear();
    }

    /*!
     * @brief Reset object and load another configuration file
     * @param path : The path to the configuration file to relocate to
     */
    void relocate(const std::string &path)
    {
        _path = path;
        clear();
        load();
    }

    /*!
     * @brief Removes associated configuration file from disk
     * @return true on success, false on failure
     */
    bool destroy()
    {
        if (getPath().empty()) throw(no_file_bound()); //No file bound to configuration
        return (destroy(_path));
    }

    /*!
     * @brief Removes a given configuration file from disk
     * @param path : The path to the configuration file to delete
     * @return true on success, false on failure
     */
    static bool destroy(const std::string &path)
    {
        if (remove(path.c_str()) != 0)
        {
            return (false); //Error on delete
        }
        return (true);
    }

    //
    // GETTERS
    //

    /*!
     * @brief Tests if a key exists in configuration
     * @param key : The key to search for
     * @return true if found, false if failed
     */
    bool exists(const std::string &key)
    {
        if (_config.find(key) != _config.end())
        {
            return (true);
        }
        return (false);
    }

    /*!
     * @brief Tests if a value equals a given param in configuration
     * @param key : The key to search for
     * @return true if equals, false if not
     */
    template <typename T>
    bool compare(const std::string &key, const T &value)
    {
        if (exists(key))
        {
            if (get<T>(key) == value)
                return (true);
        }
        return (false);
    }

    /*!
     * @brief Get any value from configuration
     * @param key : The key identifying wanted value
     * @param section : Optionnal parameter to set section.
     * @return value casted on type T
     */
    template <typename T>
    T get(const std::string &key, const std::string section = "")
    {
        T value;

        if (!get((!section.empty() ? section + SECTION_KEY_SEPARATOR + key : key), value)) throw(undefined_key(key));
        return (value);
    }

    /*!
     * @brief Get final value with substitued values if any
     * @param key : The key identifying wanted value
     * @return value with substitued key
     * DEPRECATED
     */
    std::string getFinalValue(const std::string &key)
    {
        std::string buffer = _config[key];

        for (size_t cursor = 0; cursor < buffer.length(); cursor++)
        {
			if (buffer.compare(cursor, strlen(SUBSTITUTION_SEQUENCE), SUBSTITUTION_SEQUENCE) == 0//identifier found
				&& (cursor == 0 || (cursor > 0 && buffer[cursor - 1] != ESCAPE_CHARACTER))) //check for non escaped sequence
            {
                size_t begin = cursor;
                while (cursor < buffer.length() && buffer[cursor] != ' ') cursor++;
                buffer = buffer.substr(begin, buffer.length() - cursor);
            }
        }
        return (buffer);
    }

    /*!
     * @brief Get arithmetic values from configuration
     * @param key : The key identifying wanted value
     * @param value : The arithmetic-typed variable to set with value
     * @return true if found, false if failed
     */
    template <typename T>
    bool get(const std::string &key, T &value)
    {
        if (!exists(key)) return (false);
        std::istringstream iss;
        iss.str(_config[key]);
        iss >> value;
        return (true);
    }

    /*!
     * @brief Get C-style string values from configuration
     * @param key : The key identifying wanted value
     * @param value : The char array to set with value
     * @return true if found, false if failed
     */
    bool get(const std::string &key, char *value)
    {
        if (!exists(key)) return (false);
        strcpy(value, _config[key].c_str());
        return (true);
    }

    /*!
     * @brief Get bool values from configuration
     * @param key : The key identifying wanted value
     * @param value : The bool to set with value
     * @return true if found, false if failed
     */
    bool get(const std::string &key, bool &value)
    {
        if (!exists(key)) return (false);
        value = (_config[key] == "true" ? true : false);
        return (true);
    }

    /*!
     * @brief Get string values from configuration
     * @param key : The key identifying wanted value
     * @param value : The string to set with value
     * @return true if found, false if failed
     */
    bool get(const std::string &key, std::string &value)
    {
        if (!exists(key)) return (false);
        value = _config[key];
        return (true);
    }

    /*!
     * @brief Get pair values from configuration
     * @param key : The key identifying wanted value
     * @param pair : The pair to fill with values
     * @return true if found, false if failed
     */
    template<typename Tx, typename Ty>
    bool getPair(const std::string &key,  std::pair<Tx, Ty> &pair)
    {
        if (!exists(key)) return (false);
        size_t sep = _config[key].find(VALUE_FIELD_SEPARATOR);
        if (sep != std::string::npos)
        {
            std::string buffer = _config[key];
            std::istringstream iss;

            iss.str(buffer.substr(0, sep));
            iss >> pair.first;
            iss.clear();
            iss.str(buffer.substr(sep + strlen(VALUE_FIELD_SEPARATOR),
                                        buffer.size() - sep + strlen(VALUE_FIELD_SEPARATOR)));
            iss >> pair.second;
        }
        return (true);
    }

    /*!
     * @brief Used to get container-values from configuration
     * @param key : The key identifying wanted container of values
     * @param container : The container where the container of values will be pushed
	 * @return true on success, false on failure.
     */
    template <typename T>
    bool getContainer(const std::string &key, T &container)
    {
        if (!exists(key)) return (false);
        std::istringstream iss;
        typename T::value_type value;
        std::string buffer = _config[key];

        for (size_t sep = buffer.find(VALUE_FIELD_SEPARATOR); sep != std::string::npos; sep = buffer.find(VALUE_FIELD_SEPARATOR))
        {
            iss.str(buffer.substr(0, sep));
            iss >> value;
            container.insert(container.end(), value);
            buffer.erase(0, sep + strlen(VALUE_FIELD_SEPARATOR));
            iss.clear();
        }
        iss.str(buffer);
        iss >> value;
        container.insert(container.end(), value);
        return (true);
    }


    //
    // SETTERS
    //

    /*!
     * @brief Set configuration values with arithmetic types.
     * @param key : The key indentifier to set
     * @param value : The primitive-typed value to set in key field
     */
    template <typename T>
    void set(const std::string &key, const T &value)
    {
        set(key, stringify(value));
    }

    /*!
     * @brief Set configuration values with arithmetic types.
     * @param key : The key indentifier to set
     * @param value : The primitive-typed value to set in key field
     */
    void set(const std::string &key, const std::string &value)
    {        
        if (exists(key))
        {
            _config[key] = value;
        }
        else
        {
            _config.emplace(key, value);
        }
    }

    /*!
     * @brief Set configuration values with the contents of an std::pair.
     * @param key : The key indentifier to set
     * @param pair : The pair with values to fill in key field
     */
    template<typename Tx, typename Ty>
    void setPair(const std::string &key, const std::pair<Tx, Ty> &pair)
    {
        set(key, stringify(pair.first) + VALUE_FIELD_SEPARATOR + stringify(pair.second));
    }

    /*!
     * @brief Set configuration values with the contents of any stl container implementing const_iterator.
     * @param key : The key indentifier to set
     * @param container : The container with values to fill in key field
     */
    template <typename T>
    void setContainer(const std::string &key, const T &container)
    {
        std::string fValue;

        for (typename T::const_iterator it = container.cbegin(); it != container.cend(); it++)
        {
            if (it != container.begin())
            {
                fValue += VALUE_FIELD_SEPARATOR;
            }
            fValue += stringify(*it);
        }
        set(key, fValue);
    }

    //
    // MODIFIERS
    //

    /*!
     * @brief Used to rename configuration key
     * @param srcKey : The source key containing the value to copy
     * @param destKey : The destination key fill with source value
     */
    void move(const std::string &srcKey, const std::string &destKey)
    {
        if (exists(srcKey))
        {
           copy(srcKey, destKey);
           erase(srcKey);
        }
        else
        {
            throw (undefined_key(srcKey)); //No source to move from !
        }
    }



    /*!
     * @brief Used to copy configuration value into another
     * @param srcKey : The source key containing the value to copy
     * @param destKey : The destination key fill with source value
     */
    void copy(const std::string &srcKey, const std::string &destKey)
    {
        if (exists(srcKey))
        {
            if (_config.find(destKey) != _config.end())
            {
                _config[destKey] = _config[srcKey];
            }
            else
            {
                _config.emplace(destKey, srcKey);
            }
        }
        else
        {
            throw (undefined_key(srcKey)); //No source to move from !
        }
    }

    /*!
     * @brief Erase a key from configuration
     * @param key : The key to erase
     */
    void erase(const std::string &key)
    {
        if (exists(key))
        {
            _config.erase(key);
        }
        else
        {
            throw (undefined_key(key)); //No key to erase !
        }
    }

    //
    // LOAD / SAVE
    //

    /*!
     * @brief Load config stored in the associated file.
     */
    void load()
    {
        if (getPath().empty()) throw(no_file_bound()); //No file bound to configuration
        std::ifstream file(_path, std::ifstream::in);
        std::string buffer, section;
        association pair;
        bool separator;

        if (access(_path.c_str(), F_OK) == -1) return; //No config file exists, stop loading
        else if (!file.good()) throw (bad_file(_path)); //Could not open
        while (std::getline(file, buffer)) //Read line by line
        {
            if (!formatBuffer(buffer, section)) continue; //removes comments, set section, skips invalid lines
            separator = false;
            for (size_t cursor = 0; cursor < buffer.length(); cursor++) //Parse line
            {
                if (buffer.compare(cursor, strlen(KEY_VALUE_SEPARATOR), KEY_VALUE_SEPARATOR) == 0 //sep found
                && (cursor == 0 || (cursor > 0 && buffer[cursor-1] != ESCAPE_CHARACTER))) //check for non escaped sequence
                {
                    if (!pair.first.empty()) //Separator found, we push the current key/value if valid
                    {
                        set(pair.first, pair.second);
                        pair.first.clear();
                        pair.second.clear();
                    }
                    pair.first = extractFromBuffer(buffer.substr(0, cursor));
                    if (!section.empty()) pair.first = section+SECTION_KEY_SEPARATOR+pair.first; //If section is present, concatenate with key
                    separator = true;
                    pair.second = extractFromBuffer(buffer.substr(cursor+strlen(KEY_VALUE_SEPARATOR), buffer.length() - (cursor+strlen(KEY_VALUE_SEPARATOR))));
                    break;
                }
            }
            if (separator == false) //If no separator was found, buffer belongs to previous key value
            {
                pair.second += extractFromBuffer(buffer);
            }
        }
        if (!pair.first.empty()) //Insert last bufferized key
        {
            if (!section.empty()) pair.first = section+SECTION_KEY_SEPARATOR+pair.first; //If section is present, concatenate with key before setting
            set(pair.first, pair.second);
            pair.first.clear();
            pair.second.clear();
        }
        file.close();
    }

    /*!
     * @brief Save current config state inside associated file.
     * @return true on success, false on failure.
     */
    void save()
    {
        if (getPath().empty()) throw(no_file_bound()); //No file bound to configuration
        associationMap config = _config;
        std::vector<std::string> buffer = dumpFile(), memory, serialized = buffer;
        std::ofstream file(_path, std::ofstream::out | std::ofstream::trunc);
        std::string section, prevSection, fileSection;
        association pair;

        if (!file.good())
        {
            throw(bad_file(_path)); //Couldnt open for writing
        }
        for (size_t i = 0; i < buffer.size(); i++)
        {
            if (formatBuffer(serialized[i], section))
            {
                pair = parseBuffer(serialized[i]);
                if (config.find(pair.first) != config.end())
                {
                    if (!section.empty() && section != prevSection) //We are changing section, push all remaining new keys
                    {
                        for (associationMap::iterator it = config.begin(); it != config.end(); it++)
                        {
                            if (getKeySection(it->first) == prevSection)
                            {
                                buffer.insert(std::next(buffer.begin(), static_cast<int64_t>(i)), getKeySection(it->first, false) + KEY_VALUE_SEPARATOR + it->second);
                                config.erase(it);
                            }
                        }
                    }
                    buffer[i].replace(buffer[i].find(pair.second), pair.second.length(), pair.second);
                    config.erase(pair.first);
                    prevSection = section;
                }
            }
        }
        memory = dump(config); //Extracting remaining keys from memory
        if (!memory.empty()) buffer.insert(buffer.end(), memory.begin(), memory.end());
        for (size_t i = 0; i < buffer.size(); i++) file << buffer[i] << '\n';
        file.close();
    }

    //
    // INTEROPERABILITY
    //

    /*!
     * @brief Copies a given key to another configuration
     * @param key : The key to copy
     * @param target : The target configuration to copy to
     */
    void copyTo(const std::string &key, Config &target)
    {
        if (exists(key))
        {
            target.set(key, _config[key]);
        }
        else
        {
            throw (undefined_key(key)); //No key to copy !
        }
    }

    /*!
     * @brief Moves a given key to another configuration (removes key from caller)
     * @param key : The key to move
     * @param target : The target configuration to move to
     */
    void moveTo(const std::string &key, Config &target)
    {
        copyTo(key, target);
        erase(key);
    }

    /*!
     * @brief Append the target configuration to the caller
     * @param source : The configuration to copy keys from
     */
    void append(const Config &source)
    {
        for (associationMap::const_iterator it = source._config.begin(); it != source._config.end(); it++)
        {
            set(it->first, it->second);
        }
    }

    /*!
     * @brief Append the configuration file to the caller
     * @param path : The path to the configuration file to copy keys from
     */
    void append(const std::string &path)
    {
        append(Config(path));
    }

protected:

    //
    // VALUE MANIPULATION
    //

    /*!
     * @brief Converts any value to string, correctly.
     * @param value : the T typed value to convert
     * @return a string containing the value.
     */
    template <typename T>
    std::string stringify(const T &value) 
    {
        std::string sValue;
        if (std::is_same<T, bool>::value)
        {
            sValue = (value ? "true" : "false");
        }
        else if (std::is_scalar<T>::value)
        {
            std::ostringstream out;

            if (std::is_floating_point<T>::value)
            {
                out << std::setprecision(std::numeric_limits<long double>::digits10 + 1) << value;
            }
            else
            {
                out << value;
            }
            sValue = out.str();
        }
        else
        {
            sValue = value;
        }
        return (sValue);
    }

    //
    // PARSING HELPERS
    //

    /*!
     * @brief Dump current config file into a vector buffer.
     * @return A vector buffer containing a dump of the config file.
     */
    std::vector<std::string> dumpFile() const
    {
        std::ifstream file(_path, std::ifstream::in);
        std::vector<std::string> buffer;
        std::string line;

        if (access(_path.c_str(), F_OK) == -1) return (buffer); //No config file exists, return empty buffer
        else if (!file.good()) //Could not open
        {
            throw (bad_file(_path));
        }
        while (std::getline(file, line))
            buffer.push_back(line);
        file.close();
        return (buffer);
    }

    /*!
     * @brief Dump current configuration into a vector buffer.
     * @param config : an optionnal configuration array to dump, if not specified, member will be used
     * @return A vector buffer containing a dump of the configuration.
     */
    std::vector<std::string> dump() const
    {
        return (dump(_config));
    }

    /*!
     * @brief Dump current configuration into a vector buffer.
     * @param config : an optionnal configuration array to dump, if not specified, member will be used
     * @return A vector buffer containing a dump of the configuration.
     */
    static std::vector<std::string> dump(const associationMap &config)
    {
        std::string section, prevSection, keySection;
        std::vector<std::string> buffer;

        for (associationMap::const_iterator it = config.begin(); it != config.end(); it++)
        {
            keySection = getKeySection(it->first, true);
            if (!keySection.empty()) section = keySection;
            if (section != prevSection) //We are changing section create it!
            {
                buffer.push_back(SECTION_BLOCK_BEGIN+section+SECTION_BLOCK_END);
            }
            buffer.push_back(getKeySection(it->first, false) + KEY_VALUE_SEPARATOR + it->second);
            prevSection = section;
        }
        return (buffer);
    }

    /*!
	 * @brief Filter the buffer for section
	 * @param buffer : buffer to search for section
	 * @return key or section based on param section
	 */
	static std::string parseSection(const std::string &buffer)
	{
		for (size_t begin = 0; begin < buffer.length(); begin++)
		{
			if (buffer.compare(begin, strlen(SECTION_BLOCK_BEGIN), SECTION_BLOCK_BEGIN) == 0//identifier found
				&& (begin == 0 || (begin > 0 && buffer[begin - 1] != ESCAPE_CHARACTER))) //check for non escaped sequence
			{
				for (size_t end = 0; end < buffer.length(); end++)
				{
					if (buffer.compare(end, strlen(SECTION_BLOCK_END), SECTION_BLOCK_END) == 0//identifier found
						&& (end == 0 || (end > 0 && buffer[end - 1] != ESCAPE_CHARACTER))) //check for non escaped sequence
					{
						return (buffer.substr(begin + strlen(SECTION_BLOCK_BEGIN), end - (begin + strlen(SECTION_BLOCK_BEGIN))));
					}
				}
			}
		}
		return ("");
	}

    /*!
     * @brief Filter the key for section
     * @param section : true to return section, false to return key
     * @return key or section based on param section
     */
    static std::string getKeySection(const std::string &key, bool section = true)
    {
        size_t sep;

        for (size_t cursor = 0; cursor < key.length(); cursor++)
        {
          if (key.compare(cursor, strlen(SECTION_KEY_SEPARATOR), SECTION_KEY_SEPARATOR) == 0//identifier found
          && (cursor == 0 || (cursor > 0 && key[cursor-1] != ESCAPE_CHARACTER))) //check for non escaped sequence
            {
                if (section)
                    return (key.substr(0, cursor));
                else
                    return (key.substr(cursor + strlen(SECTION_KEY_SEPARATOR), key.length() - cursor + strlen(SECTION_KEY_SEPARATOR)));
            }
        }
        if (section) return (""); //no section
		return (key);
    }

    //
    // PARSER
    //

    /*!
     * @brief Pre-treatment of file buffer: checks for comments and removes them if any. Also updates section parameter with current section.
     * @param buffer : string to parse
     * @param section : string to fill with detected section if any
     * @return true when the buffer contains a valid key/value node, false when the buffer should be ignored
     */
    static bool formatBuffer(std::string &buffer, std::string &section)
    {
        static bool inside = false;
		std::string newsection;
        size_t begin = 0;

        for (size_t cursor = 0; cursor < buffer.length(); cursor++) //Parse line
        {
            while (cursor < buffer.length() && buffer[cursor] == ' ') cursor++;
            if (cursor == buffer.length()) return (false); //If the line stops here (only space), discard.
            if (inside) //If we are inside a comment block
            {
                if (buffer.compare(cursor, strlen(COMMENT_BLOCK_END), COMMENT_BLOCK_END) == 0) //End of block found
                {
                    inside = false;
                    buffer.erase(begin, cursor + strlen(COMMENT_BLOCK_END)); //removes block
                }
            }
            else //If not inside block
            {
                if (buffer.compare(cursor, strlen(COMMENT_BLOCK_BEGIN), COMMENT_BLOCK_BEGIN) == 0) //Beginning of block found
                {
                    inside = true;
                    begin = cursor;
                }
                else //check for line separator
                {
                    for (size_t i = 0; i < strlen(COMMENT_LINE_SEPARATORS); i++)
                    {
                        if (buffer[cursor] == COMMENT_LINE_SEPARATORS[i]) //separator found
                        {
                            buffer = buffer.substr(0, cursor);
                        }
                    }
                }
            }
        }
		newsection = parseSection(buffer);
		if (!newsection.empty())
		{
			section = newsection;
			return (false);
		}
        return (!inside && buffer.length() > 0); //if we are inside a comment block or empty line, return false
    }

    /*!
     * @brief 
     * @param buffer : buffer to search for
     * @return prettified value
     */
    static std::string extractFromBuffer(const std::string &buffer)
    {
        size_t cursor = 0;
        std::pair<size_t,size_t> bounds;

        bounds.second = buffer.length();
        while (cursor < buffer.length() && buffer[cursor] == ' ') cursor++; //remove leading whitespace
        bounds.first = cursor;
        for (size_t i = 0; i < strlen(STRING_IDENTIFIERS); i++) //check for string identifiers
        {
            if (buffer[bounds.first] == STRING_IDENTIFIERS[i] //check if first char of value is identifier
            && (bounds.first == 0 || (bounds.first > 0 && buffer[bounds.first-1] != ESCAPE_CHARACTER))) //check for non escaped sequence
            {
                cursor++;
                bounds.first = cursor; //Set begin past identifier
                while (cursor < buffer.length()) //if found, while not at the end of buffer
                {
                    if (buffer[cursor] == STRING_IDENTIFIERS[i] //if we are on an identifier again
                    && (cursor == 0 || (cursor > 0 && buffer[cursor-1] != ESCAPE_CHARACTER))) //check for non escaped sequence
                    {
                        bounds.second = cursor;
                        break;
                    }
                    cursor++;
                }
                return (buffer.substr(bounds.first, bounds.second - bounds.first)); //Return detected value
            }
        }
        while (bounds.second > 0 && buffer[bounds.second - 1] == ' ') bounds.second--; //remove trailing whitespace if value is not braced with identifiers
        return (buffer.substr(bounds.first, bounds.second - bounds.first)); //Return prettified
    }

    /*!
     * @brief Check for key and value in a given string, and returns the association
     * @param line : string to parse for key/value
     * @return pair of key and value
     */
    static association parseBuffer(std::string &buffer)
    {
        association pair;
        
        for (size_t cursor = 0; cursor < buffer.length(); cursor++) //Parse line
        {
            if (buffer.compare(cursor, strlen(KEY_VALUE_SEPARATOR), KEY_VALUE_SEPARATOR) == 0 //sep found
            && (cursor == 0 || (cursor > 0 && buffer[cursor-1] != ESCAPE_CHARACTER))) //check for non escaped sequence
            {
                pair.first = extractFromBuffer(buffer.substr(0, cursor));
                pair.second = extractFromBuffer(buffer.substr(cursor+strlen(KEY_VALUE_SEPARATOR), buffer.length() - (cursor+strlen(KEY_VALUE_SEPARATOR))));
                return (pair);
            }
        }
        throw (-1); //parser error !
    }

    /*! @brief Overload for output streams, diplaying prettified configuration */
    friend std::ostream& operator<<(std::ostream& os, const Config& cfg);  
    
    //
    // MEMBERS
    //

    associationMap _config;
    std::string _path;
};

/*!
 * @brief Overload for output streams, diplaying prettified configuration
 * @param os : output stream to redirect to
 * @param cfg : configuration to display
 * @return reference to the used output stream
 */
std::ostream& operator<<(std::ostream& os, const Config& cfg)  
{
    std::vector<std::string> mem = cfg.dump();
   
    if (mem.empty()) return (os);
    for (size_t i = 0; i < mem.size(); i++) os << mem[i] << '\n';
    return (os);
}

}

#endif /* !TINYCONF_HPP_ */