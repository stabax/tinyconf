#include <iostream>
#include <vector>
#include "../include/tinyconf/tinyconf.hpp"

int main(int argc, char **argv)
{
    std::vector<int> a;
    std::cout << "##### TinyConf Parser Validator #####\n\n";
    if (argc <= 1) std::cout << "No file submitted. Usage: ./validate [file.cfg]\n\n";
    else std::cout << stb::Config(argv[1]) << "\n";
    std::cout << "##### EOF #####\n";
    return (0);
}